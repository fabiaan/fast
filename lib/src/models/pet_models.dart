import 'dart:convert';

PetModel petModelFromJson(String str) => PetModel.fromJson(json.decode(str));

String petModelToJson(PetModel data) => json.encode(data.toJson());

class PetModel {
  PetModel({
    this.id,
    this.name,
    this.gender,
    this.race,
    this.age,
    this.description,
    this.img,
  });

  String? id;
  String? name;
  String? gender;
  String? race;
  String? age;
  String? description;
  String? img;

  factory PetModel.fromJson(Map<String, dynamic> json) => PetModel(
        id: json["_id"],
        name: json["name"],
        gender: json["gender"],
        race: json["race"],
        age: json["age"],
        description: json["description"],
        img: json["img"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        "name": name,
        "gender": gender,
        "race": race,
        "age": age,
        "description": description,
        "img": img,
      };
}
