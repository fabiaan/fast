import 'package:flutter/material.dart';

class EditPetPage extends StatefulWidget {
  EditPetPage({Key? key}) : super(key: key);

  @override
  State<EditPetPage> createState() => _EditPetPageState();
}

class _EditPetPageState extends State<EditPetPage> {
  String? dropdownValue;
  var items = ['Macho', 'Hembra'];

  OutlineInputBorder errorOutlineInputBorder = OutlineInputBorder(
    borderRadius: BorderRadius.circular(10),
    borderSide: const BorderSide(
      color: Colors.red,
      width: 1.3,
    ),
  );

  OutlineInputBorder enableOutlineInputBorder = OutlineInputBorder(
    borderRadius: BorderRadius.circular(10),
    borderSide: const BorderSide(
      color: Colors.black54,
      width: 1.3,
    ),
  );

  OutlineInputBorder focusedOutlineInputBorder = OutlineInputBorder(
    borderRadius: BorderRadius.circular(10),
    borderSide: const BorderSide(
      color: Colors.blue,
      width: 1.3,
    ),
  );

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(30.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                GestureDetector(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Container(
                    width: 50,
                    height: 50,
                    alignment: Alignment.center,
                    child: const Icon(
                      Icons.arrow_back_ios_rounded,
                      color: Colors.blue,
                    ),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.blue.shade50,
                    ),
                  ),
                ),
                SizedBox(height: 20),
                Container(
                  width: width,
                  child: ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                    child: Image.network(
                      'https://i.ibb.co/2PP39fK/601eaf53c234bc1fcaa1435bee804403-1-1.png',
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                const SizedBox(height: 20),
                _name(),
                const SizedBox(height: 20),
                _gender(),
                const SizedBox(height: 20),
                _age(),
                const SizedBox(height: 20),
                _race(),
                const SizedBox(height: 20),
                _description(),
                const SizedBox(height: 20),
                _buttonPublication(),
                const SizedBox(height: 30),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _name() {
    return Container(
      child: TextFormField(
        initialValue: 'Malú',
        keyboardType: TextInputType.text,
        style: const TextStyle(
          fontSize: 20,
        ),
        decoration: InputDecoration(
          labelText: 'Nombre',
          errorBorder: errorOutlineInputBorder,
          focusedErrorBorder: errorOutlineInputBorder,
          enabledBorder: enableOutlineInputBorder,
          focusedBorder: focusedOutlineInputBorder,
        ),
      ),
    );
  }

  _gender() {
    return Container(
      child: DropdownButtonFormField(
        items: items.map((String items) {
          return DropdownMenuItem(
            value: items,
            child: Text(items),
          );
        }).toList(),
        onChanged: (String? newValue) {
          setState(() {
            dropdownValue = newValue!;
          });
        },
        decoration: InputDecoration(
          labelText: 'Género',
          labelStyle: TextStyle(
            fontSize: 20,
          ),
          errorBorder: errorOutlineInputBorder,
          focusedErrorBorder: errorOutlineInputBorder,
          enabledBorder: enableOutlineInputBorder,
          focusedBorder: focusedOutlineInputBorder,
        ),
        style: TextStyle(
          fontSize: 20,
          color: Colors.black,
        ),
        // hint: Text('Género'),
        elevation: 1,
      ),
    );
  }

  _age() {
    return Container(
      child: TextFormField(
        initialValue: '2',
        keyboardType: TextInputType.text,
        style: const TextStyle(
          fontSize: 20,
        ),
        decoration: InputDecoration(
          labelText: 'Edad',
          errorBorder: errorOutlineInputBorder,
          focusedErrorBorder: errorOutlineInputBorder,
          enabledBorder: enableOutlineInputBorder,
          focusedBorder: focusedOutlineInputBorder,
        ),
      ),
    );
  }

  _race() {
    return Container(
      child: TextFormField(
        initialValue: 'Poodle',
        keyboardType: TextInputType.text,
        style: const TextStyle(
          fontSize: 20,
        ),
        decoration: InputDecoration(
          labelText: 'Raza',
          errorBorder: errorOutlineInputBorder,
          focusedErrorBorder: errorOutlineInputBorder,
          enabledBorder: enableOutlineInputBorder,
          focusedBorder: focusedOutlineInputBorder,
        ),
      ),
    );
  }

  _description() {
    return Container(
      child: TextFormField(
        initialValue:
            'Es un perro encantador, solo quiere pasar jugando en el patio, es muy cariñoso y siempre quiere dormir junto a alguien.',
        maxLines: 3,
        keyboardType: TextInputType.multiline,
        style: const TextStyle(
          fontSize: 20,
        ),
        decoration: InputDecoration(
          alignLabelWithHint: true,
          labelText: 'Descripción',
          errorBorder: errorOutlineInputBorder,
          focusedErrorBorder: errorOutlineInputBorder,
          enabledBorder: enableOutlineInputBorder,
          focusedBorder: focusedOutlineInputBorder,
        ),
      ),
    );
  }

  _buttonPublication() {
    return Container(
      width: double.infinity,
      child: ElevatedButton(
        onPressed: () {},
        child: Text(
          'Actualizar',
          style: TextStyle(
            color: Colors.white,
            fontSize: 20,
          ),
        ),
        style: ElevatedButton.styleFrom(
          fixedSize: const Size(double.infinity, 60),
          primary: Colors.blue,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
        ),
      ),
    );
  }
}
