import 'package:flutter/material.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:flutter_svg/flutter_svg.dart';

class IntroductionPage extends StatelessWidget {
  const IntroductionPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: IntroductionScreen(
          pages: _listPages(context),
          onDone: () {
            Navigator.pushNamed(context, 'login');
          },
          onSkip: () {
            Navigator.pushNamed(context, 'login');
          },
          globalBackgroundColor: Colors.white,
          showBackButton: false,
          showSkipButton: true,
          showNextButton: false,
          skip: const Text(
            'Saltar',
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.normal,
            ),
          ),
          next: const Text(
            'Siguiente',
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.normal,
            ),
          ),
          done: const Text(
            'Comenzar',
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.normal,
            ),
          ),
          dotsDecorator: DotsDecorator(
            size: const Size.square(10.0),
            activeSize: const Size(20.0, 10.0),
            activeColor: Colors.blue,
            color: Colors.black12,
            spacing: const EdgeInsets.symmetric(horizontal: 3.0),
            activeShape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(25.0),
            ),
          ),
        ),
      ),
    );
  }

  _image(context, image) {
    double width = MediaQuery.of(context).size.width;
    return SvgPicture.asset(
      image,
      width: width,
    );
  }

  _pageViewModel(context, title, description, image) {
    return PageViewModel(
      titleWidget: Container(
        padding: const EdgeInsets.symmetric(
          vertical: 0,
          horizontal: 20,
        ),
        alignment: Alignment.centerLeft,
        child: Text(
          title,
          style: const TextStyle(
            fontSize: 25,
            color: Colors.black,
            letterSpacing: -0.5,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      bodyWidget: Container(
        padding: const EdgeInsets.symmetric(
          vertical: 0,
          horizontal: 20,
        ),
        alignment: Alignment.centerLeft,
        child: Text(
          description,
          style: const TextStyle(
            fontSize: 18,
            color: Colors.black54,
            letterSpacing: -0.5,
          ),
        ),
      ),
      image: Container(
        alignment: Alignment.bottomCenter,
        child: _image(
          context,
          image,
        ),
      ),
      decoration: const PageDecoration(
        imageAlignment: Alignment.center,
        imagePadding: EdgeInsets.symmetric(vertical: 0, horizontal: 20),
        titlePadding: EdgeInsets.only(top: 20, bottom: 8),
      ),
    );
  }

  _listPages(context) {
    return <PageViewModel>[
      _pageViewModel(
        context,
        'Bienvenido a nuestra casa de adopción animal',
        'Con nuestra aplicación harás más feliz la vida de nuestros amigos peludos.',
        'images/loving_doodle.svg',
      ),
      _pageViewModel(
        context,
        'Dale una vida digna a una mascota en adopción',
        'Te ayudaremos a elegir a una adorable mascota, salva una vida animal.',
        'images/doogie_doodle.svg',
      ),
      _pageViewModel(
        context,
        'Encontremos un hogar para tu adorable mascota',
        'Crea un perfil de tu mascota para que todos vean su actividad.',
        'images/petting_doodle.svg',
      ),
    ];
  }
}
