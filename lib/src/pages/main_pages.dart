import 'package:flutter/material.dart';
import 'package:adoption_pets/src/widgets/add_pet_widgets.dart';
import 'package:adoption_pets/src/widgets/home_widgets.dart';
import 'package:adoption_pets/src/widgets/posts_widgets.dart';
import 'package:adoption_pets/src/widgets/profile_widgets.dart';
import 'package:google_nav_bar/google_nav_bar.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';

class MainPage extends StatefulWidget {
  MainPage({Key? key}) : super(key: key);

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  int _selectedIndex = 0;

  List<Widget> _widgetOptions = <Widget>[
    HomeWidget(),
    AddPetWidget(),
    PostsWidget(),
    ProfileWidget(),
  ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color.fromARGB(255, 252, 252, 252),
        body: _widgetOptions.elementAt(_selectedIndex),
        bottomNavigationBar: Container(
          color: Colors.white,
          padding: const EdgeInsets.all(15),
          child: GNav(
            onTabChange: (index) {
              setState(() {
                _selectedIndex = index;
              });
            },
            activeColor: Colors.blue,
            tabBackgroundColor: Color.fromARGB(200, 227, 242, 253),
            padding: EdgeInsets.all(15),
            textStyle: TextStyle(
              fontSize: 16,
              color: Colors.blue,
            ),
            gap: 8,
            tabs: [
              GButton(
                iconColor: Colors.black54,
                iconSize: 28,
                icon: FeatherIcons.home,
                text: 'Inicio',
              ),
              GButton(
                iconColor: Colors.black54,
                iconSize: 28,
                icon: FeatherIcons.heart,
                text: 'Publicar',
              ),
              GButton(
                iconColor: Colors.black54,
                iconSize: 28,
                icon: FeatherIcons.bookmark,
                text: 'Posts',
              ),
              GButton(
                iconColor: Colors.black54,
                iconSize: 28,
                icon: FeatherIcons.settings,
                text: 'Perfil',
              ),
            ],
          ),
        ),
      ),
    );
  }
}
