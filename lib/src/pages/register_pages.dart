import 'package:adoption_pets/src/widgets/button_loader_widgets.dart';
import 'package:flutter/material.dart';
import 'package:adoption_pets/src/providers/user_providers.dart';
import 'package:adoption_pets/src/models/user_models.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final _formKey = GlobalKey<FormState>();
  final userProvider = UserProvider();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  UserModel user = UserModel();
  bool _isVisible = false;
  bool _isLoading = false;

  _viewPassword() {
    setState(() {
      _isVisible = !_isVisible;
    });
  }

  OutlineInputBorder errorOutlineInputBorder = OutlineInputBorder(
    borderRadius: BorderRadius.circular(10),
    borderSide: const BorderSide(
      color: Colors.red,
      width: 1.3,
    ),
  );

  OutlineInputBorder enableOutlineInputBorder = OutlineInputBorder(
    borderRadius: BorderRadius.circular(10),
    borderSide: const BorderSide(
      color: Colors.black54,
      width: 1.3,
    ),
  );

  OutlineInputBorder focusedOutlineInputBorder = OutlineInputBorder(
    borderRadius: BorderRadius.circular(10),
    borderSide: const BorderSide(
      color: Colors.blue,
      width: 1.3,
    ),
  );

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
          child: Column(children: <Widget>[
            _container(_isVisible, _viewPassword),
          ]),
        ),
      ),
    );
  }

  _container(_isVisible, _viewPassword) {
    return Container(
      padding: const EdgeInsets.all(40),
      child: Form(
        key: _formKey,
        child: Column(
          children: <Widget>[
            _leftButton(),
            const SizedBox(height: 30),
            _subtitle(),
            const SizedBox(height: 30),
            _nameInput(),
            const SizedBox(height: 20),
            _phoneInput(),
            const SizedBox(height: 20),
            _ubicationInput(),
            const SizedBox(height: 20),
            _emailInput(),
            const SizedBox(height: 20),
            _passwordInput(_isVisible, _viewPassword),
            const SizedBox(height: 20),
            _registerButton(),
            const SizedBox(height: 20),
            _footer()
          ],
        ),
      ),
    );
  }

  _logo() {
    return Container(
      child: const Text(
        'Pethouse',
        style: TextStyle(
          fontSize: 30,
          fontWeight: FontWeight.w900,
          color: Colors.blue,
          letterSpacing: -0.5,
        ),
      ),
    );
  }

  _subtitle() {
    return Container(
      alignment: Alignment.centerLeft,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: const <Widget>[
          Text(
            'Crea una nueva cuenta',
            style: TextStyle(
              fontSize: 25,
              color: Colors.black,
              letterSpacing: -0.5,
              fontWeight: FontWeight.bold,
            ),
          ),
          Text(
            'Para acceder al contenido',
            style: TextStyle(
              fontSize: 18,
              color: Colors.black54,
              letterSpacing: -0.5,
            ),
          ),
        ],
      ),
    );
  }

  _emailInput() {
    return Container(
      child: TextFormField(
        onSaved: (value) => user.email = value,
        validator: (value) {
          return _validateWithRegex(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+",
            value,
            "El campo debe tener un correo válido",
          );
        },
        keyboardType: TextInputType.emailAddress,
        style: const TextStyle(
          fontSize: 20,
        ),
        decoration: InputDecoration(
          labelText: 'Correo',
          errorBorder: errorOutlineInputBorder,
          focusedErrorBorder: errorOutlineInputBorder,
          enabledBorder: enableOutlineInputBorder,
          focusedBorder: focusedOutlineInputBorder,
        ),
      ),
    );
  }

  _passwordInput(_isVisible, _viewPassword) {
    return Container(
      child: TextFormField(
        onSaved: (value) => user.password = value,
        validator: (value) {
          return _validateWithRegex(
            r"^.{8,}$",
            value,
            "El campo debe tener al menos 8 caracteres",
          );
        },
        obscureText: !_isVisible,
        style: const TextStyle(
          fontSize: 20,
        ),
        decoration: InputDecoration(
          labelText: 'Contraseña',
          errorBorder: errorOutlineInputBorder,
          focusedErrorBorder: errorOutlineInputBorder,
          enabledBorder: enableOutlineInputBorder,
          focusedBorder: focusedOutlineInputBorder,
          suffixIcon: IconButton(
            onPressed: () {
              _viewPassword();
            },
            icon: _isVisible
                ? const Icon(FeatherIcons.eye)
                : const Icon(FeatherIcons.eyeOff),
          ),
        ),
      ),
    );
  }

  _nameInput() {
    return Container(
      child: TextFormField(
        onSaved: (value) => user.name = value,
        validator: (value) {
          return _validateWithRegex(
            r"^([a-zA-Zñ ]){3,25}$",
            value,
            "El campo debe tener un nombre válido",
          );
        },
        keyboardType: TextInputType.text,
        style: const TextStyle(
          fontSize: 20,
        ),
        decoration: InputDecoration(
          labelText: 'Nombre',
          errorBorder: errorOutlineInputBorder,
          focusedErrorBorder: errorOutlineInputBorder,
          enabledBorder: enableOutlineInputBorder,
          focusedBorder: focusedOutlineInputBorder,
        ),
      ),
    );
  }

  _phoneInput() {
    return Container(
      child: TextFormField(
        onSaved: (value) => user.phone = value,
        validator: (value) {
          return _validateWithRegex(
            r"^\d{10}$",
            value,
            "El campo debe tener un teléfono válido",
          );
        },
        keyboardType: TextInputType.phone,
        style: const TextStyle(
          fontSize: 20,
        ),
        decoration: InputDecoration(
          labelText: 'Teléfono',
          errorBorder: errorOutlineInputBorder,
          focusedErrorBorder: errorOutlineInputBorder,
          enabledBorder: enableOutlineInputBorder,
          focusedBorder: focusedOutlineInputBorder,
        ),
      ),
    );
  }

  _ubicationInput() {
    return Container(
      child: TextFormField(
        onSaved: (value) => user.location = value,
        validator: (value) {
          return _validateWithRegex(
            r"^.{5,}$",
            value,
            "El campo debe tener al menos 5 caracteres",
          );
        },
        keyboardType: TextInputType.text,
        style: const TextStyle(
          fontSize: 20,
        ),
        decoration: InputDecoration(
          labelText: 'Ubicación',
          errorBorder: errorOutlineInputBorder,
          focusedErrorBorder: errorOutlineInputBorder,
          enabledBorder: enableOutlineInputBorder,
          focusedBorder: focusedOutlineInputBorder,
        ),
      ),
    );
  }

  _registerButton() {
    return _isLoading
        ? const ButtonLoaderWidget()
        : Container(
            child: ElevatedButton(
              onPressed: _submit,
              child: const Text(
                'Regístrarse',
                style: TextStyle(
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
              style: ElevatedButton.styleFrom(
                fixedSize: const Size(1000, 60),
                primary: Colors.blue,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
            ),
          );
  }

  _footer() {
    return Container(
      alignment: Alignment.center,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          const Text(
            '¿Tienes una cuenta?',
            style: TextStyle(
              fontSize: 18,
              color: Colors.black54,
              letterSpacing: -0.5,
            ),
          ),
          TextButton(
            onPressed: () {
              Navigator.pushNamed(context, 'login');
            },
            child: const Text(
              'Inicia sesión',
              style: TextStyle(
                fontSize: 18,
                color: Colors.blue,
                letterSpacing: -0.5,
              ),
            ),
          ),
        ],
      ),
    );
  }

  _leftButton() {
    return Container(
      alignment: Alignment.centerLeft,
      child: Ink(
        child: IconButton(
          splashRadius: 25,
          icon: const Icon(
            Icons.arrow_back_ios_rounded,
            color: Colors.blue,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        decoration: ShapeDecoration(
          color: Colors.blue.shade50,
          shape: const CircleBorder(),
        ),
      ),
    );
  }

  _validateWithRegex(regex, value, message) {
    if (value.isEmpty) return 'Este campo es requerido';
    if (!RegExp(regex).hasMatch(value)) return message;
    return null;
  }

  void _submit() async {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();

      setState(() => _isLoading = true);

      final response = await userProvider.register(user);
      final status = response["status"];

      if (status != 500 && status != 400) {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text('¡Te has registrado correctamente!'),
            backgroundColor: Colors.green,
          ),
        );

        Navigator.pushNamed(context, 'login');
        await Future.delayed(const Duration(seconds: 2));

        setState(() => _isLoading = false);
      } else {
        setState(() => _isLoading = false);
        final message = response["message"];
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            content: Text(message),
            backgroundColor: Colors.red,
          ),
        );
      }
    }
  }
}
