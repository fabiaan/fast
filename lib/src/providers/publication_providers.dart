import 'dart:convert';

import 'package:adoption_pets/src/models/publication_models.dart';
import 'package:http/http.dart' as http;

class PublicationProvider {
  final String _baseUrl = 'api-adoption-pets.onrender.com';
  final Map<String, String> headers = {
    'Content-Type': 'application/json; charset=UTF-8',
  };

  Future<List<PublicationModel>> getPublications() async {
    const String _pathUrl = 'v1/api/publication';
    final url = Uri.https(_baseUrl, _pathUrl);

    final response = await http.get(url, headers: headers);
    final decodeData = json.decode(response.body);
    final List<PublicationModel> publications = [];

    if (decodeData == null) return [];

    decodeData.forEach((element) {
      final publicationTemp = PublicationModel.fromJson(element);
      publications.add(publicationTemp);
    });

    return publications;
  }
}
