import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:adoption_pets/src/models/user_models.dart';

class UserProvider {
  final String _baseUrl = 'api-adoption-pets.onrender.com';
  final Map<String, String> headers = {
    'Content-Type': 'application/json; charset=UTF-8',
  };

  register(UserModel user) async {
    const String _pathUrl = 'v1/api/auth/signup';
    final url = Uri.https(_baseUrl, _pathUrl);

    final response = await http.post(
      url,
      headers: headers,
      body: userModelToJson(user),
    );
    final decodeData = json.decode(response.body);

    return decodeData;
  }
}
