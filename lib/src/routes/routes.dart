import 'package:flutter/material.dart';
import 'package:adoption_pets/src/pages/edit_pet_pages.dart';
import 'package:adoption_pets/src/pages/pet_pages.dart';
import 'package:adoption_pets/src/pages/introduction_pages.dart';
import 'package:adoption_pets/src/pages/main_pages.dart';
import 'package:adoption_pets/src/pages/register_pages.dart';
import 'package:adoption_pets/src/pages/login_pages.dart';

Map<String, WidgetBuilder> getApplicationRoutes() {
  return <String, WidgetBuilder>{
    'login': (BuildContext context) => LoginPage(),
    'register': (BuildContext context) => RegisterPage(),
    'intro': (BuildContext context) => const IntroductionPage(),
    'main': (BuildContext context) => MainPage(),
    'pet': (BuildContext context) => const PetPage(),
    'edit': (BuildContext context) => EditPetPage(),
  };
}
