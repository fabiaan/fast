import 'package:flutter/material.dart';

class AddPetWidget extends StatefulWidget {
  AddPetWidget({Key? key}) : super(key: key);

  @override
  State<AddPetWidget> createState() => _AddPetWidgetState();
}

class _AddPetWidgetState extends State<AddPetWidget> {
  String? dropdownValue;
  var items = ['Macho', 'Hembra'];

  OutlineInputBorder errorOutlineInputBorder = OutlineInputBorder(
    borderRadius: BorderRadius.circular(10),
    borderSide: const BorderSide(
      color: Colors.red,
      width: 1.3,
    ),
  );

  OutlineInputBorder enableOutlineInputBorder = OutlineInputBorder(
    borderRadius: BorderRadius.circular(10),
    borderSide: const BorderSide(
      color: Colors.black54,
      width: 1.3,
    ),
  );

  OutlineInputBorder focusedOutlineInputBorder = OutlineInputBorder(
    borderRadius: BorderRadius.circular(10),
    borderSide: const BorderSide(
      color: Colors.blue,
      width: 1.3,
    ),
  );

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          _header(),
          _body(),
        ],
      ),
    );
  }

  _header() {
    return Container(
      padding: const EdgeInsets.all(30),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: const <Widget>[
              Text(
                'Publica',
                style: TextStyle(
                  fontSize: 25,
                  color: Colors.black,
                  letterSpacing: -0.5,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
          const Text(
            'Una mascota en adopción',
            style: TextStyle(
              fontSize: 18,
              color: Colors.black54,
              letterSpacing: -0.5,
            ),
          ),
        ],
      ),
    );
  }

  _body() {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 0, horizontal: 30),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _buttonAddImage(),
          const SizedBox(height: 20),
          _name(),
          const SizedBox(height: 20),
          _gender(),
          const SizedBox(height: 20),
          _age(),
          const SizedBox(height: 20),
          _race(),
          const SizedBox(height: 20),
          _description(),
          const SizedBox(height: 20),
          _buttonPublication(),
          const SizedBox(height: 30),
        ],
      ),
    );
  }

  _name() {
    return Container(
      child: TextFormField(
        keyboardType: TextInputType.text,
        style: const TextStyle(
          fontSize: 20,
        ),
        decoration: InputDecoration(
          labelText: 'Nombre',
          errorBorder: errorOutlineInputBorder,
          focusedErrorBorder: errorOutlineInputBorder,
          enabledBorder: enableOutlineInputBorder,
          focusedBorder: focusedOutlineInputBorder,
        ),
      ),
    );
  }

  _gender() {
    return Container(
      // decoration: BoxDecoration(
      //   border: Border.all(
      //     color: Colors.black54,
      //     width: 1.3,
      //   ),
      //   borderRadius: BorderRadius.all(Radius.circular(10.0)),
      // ),
      // child: Padding(
      //   padding: EdgeInsets.symmetric(horizontal: 12, vertical: 10),
      //   child: DropdownButton(
      //     value: dropdownValue,
      //     isExpanded: true,
      //     items: items.map((String items) {
      //       return DropdownMenuItem(
      //         value: items,
      //         child: Text(items),
      //       );
      //     }).toList(),
      //     onChanged: (String? newValue) {
      //       setState(() {
      //         dropdownValue = newValue!;
      //       });
      //     },
      //     underline: Container(),
      //     style: TextStyle(
      //       fontSize: 20,
      //       color: Colors.black54,
      //     ),
      //     elevation: 1,
      //     hint: Text('Género'),
      //   ),
      // ),
      child: DropdownButtonFormField(
        items: items.map((String items) {
          return DropdownMenuItem(
            value: items,
            child: Text(items),
          );
        }).toList(),
        onChanged: (String? newValue) {
          setState(() {
            dropdownValue = newValue!;
          });
        },
        decoration: InputDecoration(
          labelText: 'Género',
          labelStyle: TextStyle(
            fontSize: 20,
          ),
          errorBorder: errorOutlineInputBorder,
          focusedErrorBorder: errorOutlineInputBorder,
          enabledBorder: enableOutlineInputBorder,
          focusedBorder: focusedOutlineInputBorder,
        ),
        style: TextStyle(
          fontSize: 20,
          color: Colors.black,
        ),
        // hint: Text('Género'),
        elevation: 1,
      ),
    );
  }

  _age() {
    return Container(
      child: TextFormField(
        keyboardType: TextInputType.text,
        style: const TextStyle(
          fontSize: 20,
        ),
        decoration: InputDecoration(
          labelText: 'Edad',
          errorBorder: errorOutlineInputBorder,
          focusedErrorBorder: errorOutlineInputBorder,
          enabledBorder: enableOutlineInputBorder,
          focusedBorder: focusedOutlineInputBorder,
        ),
      ),
    );
  }

  _race() {
    return Container(
      child: TextFormField(
        keyboardType: TextInputType.text,
        style: const TextStyle(
          fontSize: 20,
        ),
        decoration: InputDecoration(
          labelText: 'Raza',
          errorBorder: errorOutlineInputBorder,
          focusedErrorBorder: errorOutlineInputBorder,
          enabledBorder: enableOutlineInputBorder,
          focusedBorder: focusedOutlineInputBorder,
        ),
      ),
    );
  }

  _description() {
    return Container(
      child: TextFormField(
        maxLines: 3,
        keyboardType: TextInputType.multiline,
        style: const TextStyle(
          fontSize: 20,
        ),
        decoration: InputDecoration(
          alignLabelWithHint: true,
          labelText: 'Descripción',
          errorBorder: errorOutlineInputBorder,
          focusedErrorBorder: errorOutlineInputBorder,
          enabledBorder: enableOutlineInputBorder,
          focusedBorder: focusedOutlineInputBorder,
        ),
      ),
    );
  }

  _buttonAddImage() {
    return Container(
      width: double.infinity,
      child: ElevatedButton(
        onPressed: () {},
        child: Text(
          'Añadir imagen',
          style: TextStyle(
            color: Colors.white,
            fontSize: 20,
          ),
        ),
        style: ElevatedButton.styleFrom(
          fixedSize: const Size(double.infinity, 60),
          primary: Colors.blue,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
        ),
      ),
    );
  }

  _buttonPublication() {
    return Container(
      width: double.infinity,
      child: ElevatedButton(
        onPressed: () {},
        child: Text(
          'Publicar',
          style: TextStyle(
            color: Colors.white,
            fontSize: 20,
          ),
        ),
        style: ElevatedButton.styleFrom(
          fixedSize: const Size(double.infinity, 60),
          primary: Colors.blue,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
        ),
      ),
    );
  }
}
