import 'package:adoption_pets/src/models/publication_models.dart';
import 'package:adoption_pets/src/providers/publication_providers.dart';
import 'package:flutter/material.dart';

class HomeWidget extends StatefulWidget {
  HomeWidget({Key? key}) : super(key: key);

  @override
  State<HomeWidget> createState() => _HomeWidgetState();
}

class _HomeWidgetState extends State<HomeWidget> {
  final publicationProvider = PublicationProvider();

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          _header(),
          _body(),
        ],
      ),
    );
  }

  _header() {
    return Container(
      padding: const EdgeInsets.all(30),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: const <Widget>[
              Text(
                'Explora',
                style: TextStyle(
                  fontSize: 25,
                  color: Colors.black,
                  letterSpacing: -0.5,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
          const Text(
            'Los animales en adopción',
            style: TextStyle(
              fontSize: 18,
              color: Colors.black54,
              letterSpacing: -0.5,
            ),
          ),
        ],
      ),
    );
  }

  _body() {
    return SingleChildScrollView(
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 0, horizontal: 30),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [_listPublications()],
        ),
      ),
    );
  }

  _listPublications() {
    return FutureBuilder(
      future: publicationProvider.getPublications(),
      builder: (BuildContext context,
          AsyncSnapshot<List<PublicationModel>> snapshot) {
        if (snapshot.hasData) {
          final publications = snapshot.data;
          return ListView.builder(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            itemCount: publications?.length,
            itemBuilder: (context, item) {
              return _createPublication(publications![item]);
            },
          );
        } else {
          return _skeletonContainer();
        }
      },
    );
  }

  _createPublication(PublicationModel publication) {
    final name = publication.pet?.name;
    final age = int.tryParse(publication.pet?.age ?? '0');
    final gender = publication.pet?.gender;
    final race = publication.pet?.race;
    final img = publication.pet?.img;
    final location = publication.user?.location;
    final active = publication.state;

    return GestureDetector(
      onTap: () {
        Navigator.of(context).pushNamed('pet');
      },
      child: Container(
        decoration: const BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
          color: Color.fromARGB(200, 227, 242, 253),
        ),
        child: Row(
          children: [
            Container(
              padding: const EdgeInsets.all(10),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Image.network(
                  '$img',
                  width: 150.0,
                ),
              ),
            ),
            Container(
              height: 150.0,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      Container(
                        child: Text(
                          '$name',
                          style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      const SizedBox(width: 5),
                      Container(
                        child: Icon(
                          gender == 'Macho'
                              ? Icons.male_rounded
                              : Icons.female_rounded,
                          color: Colors.blue,
                          size: 20,
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Container(
                        child: Text(
                          'Raza: ',
                          style: TextStyle(
                            fontSize: 16,
                            color: Colors.black54,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Container(
                        child: Text(
                          '$race',
                          style: TextStyle(fontSize: 16, color: Colors.black54),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Container(
                        child: Text(
                          'Edad: ',
                          style: TextStyle(
                            fontSize: 16,
                            color: Colors.black54,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Container(
                        child: Text(
                          age! > 1 ? '$age años' : '$age año',
                          style: TextStyle(fontSize: 16, color: Colors.black54),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Container(
                        child: Text(
                          'Lugar: ',
                          style: TextStyle(
                            fontSize: 16,
                            color: Colors.black54,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Container(
                        child: Text(
                          '$location',
                          style: TextStyle(fontSize: 16, color: Colors.black54),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Container(
                        child: Text(
                          'Estado: ',
                          style: TextStyle(
                            fontSize: 16,
                            color: Colors.black54,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      Container(
                        child: Text(
                          active! ? 'Activo' : 'Inactivo',
                          style: TextStyle(fontSize: 16, color: Colors.black54),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  _skeletonContainer() {
    return AnimatedOpacity(
      opacity: 0.3,
      duration: const Duration(milliseconds: 500),
      child: Container(
        width: double.infinity,
        height: 170,
        decoration: const BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(10.0)),
          color: Colors.black12,
        ),
        child: Row(
          children: [
            Container(
              padding: const EdgeInsets.all(10),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(10),
                child: Container(
                  color: Colors.black26,
                  width: 150.0,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
