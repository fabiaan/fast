import 'package:flutter/material.dart';

class ProfileWidget extends StatefulWidget {
  ProfileWidget({Key? key}) : super(key: key);

  @override
  State<ProfileWidget> createState() => _ProfileWidgetState();
}

class _ProfileWidgetState extends State<ProfileWidget> {
  final TextStyle _textStyleTitle = const TextStyle(
    fontSize: 20,
    fontWeight: FontWeight.bold,
    color: Colors.blue,
    letterSpacing: -0.5,
  );

  final TextStyle _textStyleData = const TextStyle(
    fontSize: 18,
    color: Colors.black54,
    letterSpacing: -0.5,
  );

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          _header(),
          _body(),
        ],
      ),
    );
  }

  _header() {
    return Container(
      padding: const EdgeInsets.all(30),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: const <Widget>[
              Text(
                'Configura',
                style: TextStyle(
                  fontSize: 25,
                  color: Colors.black,
                  letterSpacing: -0.5,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ],
          ),
          const Text(
            'Los datos de tu cuenta',
            style: TextStyle(
              fontSize: 18,
              color: Colors.black54,
              letterSpacing: -0.5,
            ),
          ),
        ],
      ),
    );
  }

  _body() {
    return SingleChildScrollView(
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 0, horizontal: 30),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                color: Colors.blue.shade50,
                shape: BoxShape.circle,
              ),
              padding: const EdgeInsets.all(40),
              child: Text(
                'J',
                style: TextStyle(
                  fontSize: 30,
                  color: Colors.blue,
                ),
              ),
            ),
            const SizedBox(height: 20),
            Container(
              width: double.infinity,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Nombre',
                    style: _textStyleTitle,
                  ),
                  const SizedBox(height: 5),
                  Padding(
                    padding: EdgeInsets.only(left: 10),
                    child: Text(
                      'Juan Cevallos',
                      style: _textStyleData,
                    ),
                  ),
                  _buildDivider(),
                  Text(
                    'Correo',
                    style: _textStyleTitle,
                  ),
                  const SizedBox(height: 5),
                  Padding(
                    padding: EdgeInsets.only(left: 10),
                    child: Text(
                      'juanc@gmail.com',
                      style: _textStyleData,
                    ),
                  ),
                  _buildDivider(),
                  Text(
                    'Teléfono',
                    style: _textStyleTitle,
                  ),
                  const SizedBox(height: 5),
                  Padding(
                    padding: EdgeInsets.only(left: 10),
                    child: Text(
                      '0987654322',
                      style: _textStyleData,
                    ),
                  ),
                  _buildDivider(),
                  Text(
                    'Dirección',
                    style: _textStyleTitle,
                  ),
                  const SizedBox(height: 5),
                  Padding(
                    padding: EdgeInsets.only(left: 10),
                    child: Text(
                      'Manta, Ecuador',
                      style: _textStyleData,
                    ),
                  ),
                  const SizedBox(height: 30),
                  Row(
                    children: [
                      Expanded(
                        child: Container(
                          width: double.infinity,
                          child: ElevatedButton(
                            onPressed: () {},
                            child: Text(
                              'Cerrar sesión',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 20,
                              ),
                            ),
                            style: ElevatedButton.styleFrom(
                              fixedSize: const Size(double.infinity, 60),
                              primary: Colors.blue,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  _buildDivider() {
    return Column(
      children: [
        const SizedBox(height: 10),
        Divider(color: Colors.black.withOpacity(0.3)),
        const SizedBox(height: 10),
      ],
    );
  }
}
